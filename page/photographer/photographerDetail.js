layui.config({
    base: "../../js/"
}).extend({
    "utilObj": "utilObj"
});
layui.use(['form', 'utilObj', 'laydate', 'layer'], function () {
    var form = layui.form
    layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        utilObj = layui.utilObj;
});