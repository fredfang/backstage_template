layui.config({
    base: "../../js/"
}).extend({
    "utilObj": "utilObj"
});
layui.use(['utilObj', 'form', 'layer', 'laydate', 'table', 'laytpl', 'laypage', 'util'], function () {
    var utilObj = layui.utilObj;//自定义模块，包含ajax
    var laypage = layui.laypage;
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;
    var getPhotographerPageUrl = "/photographer/getPhotographerByName";
    var changeStatus = "/photographer/changeStatus";
    var photographerPage = {
        getData: function (pageIndex) {
            var name = $("input[name='name']").val();
            var enname = $("input[name='enname']").val();
            utilObj.showLoading();
            utilObj.ajax({
                url: getPhotographerPageUrl,
                data: {
                    enname: enname,
                    name: name,
                    pageSize: utilObj.pageSize,
                    pageIndex: pageIndex - 1
                },
                success: function (data) {
                    if (data && data.content) {
                        console.log(data);
                        storePage.tableIns = table.render({
                            elem: '#photographer-list',
                            cellMinWidth: 95,
                            page: false,
                            limit: utilObj.pageSize,
                            height: "full-225",
                            id: "photographerListTable",
                            cols: [[
                                {
                                    field: 'name',
                                    title: '中文名称',
                                    align: "center",
                                    width: 300
                                },
                                {
                                    field: 'enname',
                                    title: '英文名称',
                                    align: "center",
                                    width: 300
                                },
                                {
                                    field: 'avatar',
                                    title: '图片',
                                    minWidth: 80,
                                    event: "imagesEvent",
                                    align: 'center',
                                    templet: function (d) {
                                        var r = '';
                                        if (d.avatar) {
                                            r = '<div img="' + d.avatar + '" class="table-cell-image" style="background-image:url(' + d.avatar + ')"></div>';
                                        }
                                        return r;
                                    }
                                },
                                {
                                    field: 'returnAll',
                                    title: '是否返回全部',
                                    align: 'center',
                                    width: 120,
                                    templet: function (d) {
                                        if (d.returnAll == 1) {
                                            return "是";
                                        } else if (d.returnAll == 0) {
                                            return "否";
                                        }
                                    }
                                },
                                {
                                    field: 'status',
                                    title: '状态',
                                    align: 'center',
                                    width: 120,
                                    templet: function (d) {
                                        if (d.status == 1) {
                                            return "是";
                                        } else if (d.status == 0) {
                                            return "否";
                                        }
                                    }
                                },
                                {
                                    title: '操作',
                                    width: 170,
                                    fixed: "right",
                                    align: "center",
                                    templet: function (d) {
                                        var result = "<a class=\"layui-btn layui-btn-xs \" id=\"btn-detail\" lay-event=\"detail\">查看</a>";
                                        // if (utilObj.getUser().role == 1) {
                                        result += "<a class=\"layui-btn layui-btn-xs\" id=\"btn-edit\" lay-event=\"edit\">编辑</a>";
                                        if (d.status == 1) {
                                            result += "<a class=\"layui-btn layui-btn-xs layui-btn-warm\" id=\"btn-disabled\" lay-event=\"disabled\">禁用</a>";
                                        } else if (d.status == 0) {
                                            result += "<a class=\"layui-btn layui-btn-xs layui-btn-warm\" id=\"btn-enabled\" lay-event=\"enabled\">启用</a>";
                                        }
                                        // }
                                        return result;
                                    }
                                }
                            ]],
                            data: data.content
                        });
                        laypage.render({
                            elem: 'photographerPagination',
                            curr: pageIndex,
                            count: data.totalElements,
                            limit: utilObj.pageSize,
                            jump: function (obj, first) {
                                photographerPage.current = obj.curr;
                                if (!first) {
                                    photographerPage.getData(obj.curr);
                                }
                            }
                        });
                    }
                    utilObj.hideLoading();
                },
                error: function () {
                    utilObj.hideLoading();
                }
            });
        },
        getPhotographerDetail: function (data, operator) {
            if (operator === 0) {
                openPhotographerDetail(data);
            } else if (operator === 1) {
                editPhotographerDetail(data);
            }
        },
        changeStatus: function (userId, storeId, requestUrl) {
            var requestData = {
                userId: userId,
                storeId: storeId
            };
            utilObj.showLoading();
            utilObj.ajax({
                url: requestUrl,
                data: requestData,
                success: function (data) {
                    if (data) {
                        storePage.getData(storePage.current);
                    }
                    utilObj.hideLoading();
                },
                error: function () {
                    utilObj.hideLoading();
                }
            });
        },
        init: function () {
            laydate.render({
                elem: '#startTime', type: 'date'
            });
            laydate.render({
                elem: '#endTime', type: 'date'
            });
            // if (utilObj.getUser().role == 1) {
            $(".btn-addnew").show();
            // } else {
            //     $(".btn-addnew").hide();
            // }
            // storePage.getData(1);
            // storePage.initCountryCity();
            table.on('tool(store-list)', function (obj) {
                var layEvent = obj.event,
                    data = obj.data;
                if (layEvent === 'detail') {
                    storePage.getPhotographerDetail(data, 0);
                } else if (layEvent === 'edit') {
//                  if (data.isvalid) {
//                      layer.open({
//                          title: '提示',
//                          content: '请先禁用店铺再进行编辑'
//                      });
//                      return;
//                  } else {
//                  }
                    storePage.getPhotographerDetail(data, 1);
                } else if (layEvent === 'disabled') {
                    var usableText = "是否确定禁用此店铺？"
                    layer.confirm(usableText, {
                        icon: 3,
                        title: '系统提示',
                        cancel: function (index) {
                            layer.close(index);
                        }
                    }, function (index) {
                        layer.close(index);
                        var user = localStorage.getItem("user");
                        if (user) {
                            user = JSON.parse(user);
                            storePage.changeStoreState(user.userId, data.id, invalidStoreUrl);
                        }
                    }, function (index) {
                        layer.close(index);
                    });
                } else if (layEvent === 'enabled') {
                    var usableText = "是否确定启用此店铺？"
                    layer.confirm(usableText, {
                        icon: 3,
                        title: '系统提示',
                        cancel: function (index) {
                            layer.close(index);
                        }
                    }, function (index) {
                        layer.close(index);
                        var user = localStorage.getItem("user");
                        if (user) {
                            user = JSON.parse(user);
                            storePage.changeStoreState(user.userId, data.id, validStoreUrl);
                        }
                    }, function (index) {
                        layer.close(index);
                    });
                }
            });
        }
    };
    photographerPage.init();

    $(".btn_search").on('click', function () {
        photographerPage.getData(1);
    });

    $(".btn-addnew").on('click', function () {
        editPhotographerDetail();
    });


    function openPhotographerDetail(data) {
        if (data) {
            $(".storeId").val(data.id);
            var title = "店铺详情";
            var index = layui.layer.open({
                title: title,
                type: 2,
                content: "storeDetail.html",
                success: function (layero, index) {
                    var body = layui.layer.getChildFrame('body', index);
                    if (data) {
                        // store-id
                        body.find("#store-id").val(data.id);  //用户id
                        body.find(".storeName").text(data.name);
                        var storeType = "";
                        if (data.type == 1) {
                            storeType = "专卖店";
                        } else if (data.type == 2) {
                            storeType = "授权经销商";
                        } else if (data.type == 3) {
                            storeType = "临时展台";
                        }
                        body.find(".storeType").text(storeType);
                        if (data.type == 3) {
                            body.find(".storeTime").show();
                            body.find(".fromTime").text(data.startTime);
                            body.find(".toTime").text(data.endTime);
                        } else {
                            body.find(".storeTime").hide();
                        }
                        var canAppoint = data.canAppoint ? "可预约" : "不可预约";
                        body.find(".canAppoint").text(canAppoint);

                        if (data.salesList) {
                            var saleList = data.salesList.split(',');
                            for (var index in saleList) {
                                if (saleList[index] == 1) {
                                    body.find("input:checkbox[value='watch']").attr("checked", "checked").attr("disabled", "true");
                                } else if (saleList[index] == 2) {
                                    body.find("input:checkbox[value='jewelry']").attr("checked", "checked").attr("disabled", "true");
                                } else if (saleList[index] == 3) {
                                    body.find("input:checkbox[value='bag']").attr("checked", "checked").attr("disabled", "true");
                                } else if (saleList[index] == 4) {
                                    body.find("input:checkbox[value='perfume']").attr("checked", "checked").attr("disabled", "true");
                                } else if (saleList[index] == 5) {
                                    body.find("input:checkbox[value='glasses']").attr("checked", "checked").attr("disabled", "true");
                                }
                            }
                        }
                        body.find("input:checkbox[value='watch']").attr("disabled", "true");
                        body.find("input:checkbox[value='jewelry']").attr("disabled", "true");
                        body.find("input:checkbox[value='bag']").attr("disabled", "true");
                        body.find("input:checkbox[value='perfume']").attr("disabled", "true");
                        body.find("input:checkbox[value='glasses']").attr("disabled", "true");

                        body.find(".storeEnglishName").text(data.nameEn);
                        body.find(".storeContact").text(data.contact);
                        body.find(".contactNumber").text(data.contactNumber);
                        body.find(".storePhone").text(data.phone);
                        var bannerContent = '';
                        for (var i = 1; i <= 5; i++) {
                            if (data['image' + i]) {
                                bannerContent += '<div img="' + d["image" + i] + '" class="table-cell-image" style="background-image:url(' + d["image" + i] + ')"></div>';
                            }
                        }
                        body.find("#store-banner").innerHeight = bannerContent;

                        body.find(".event-title").text(data.eventTitle);
                        body.find(".event-englishtitle").text(data.eventTitleEn);
                        body.find(".event-url").text(data.eventUrl);

                        body.find(".storeCountry").text(data.country);
                        body.find(".storeCity").text(data.city);
                        body.find(".storeAddress").text(data.address);
                        body.find(".storeEnglishAddress").text(data.addressEn);

                        var iframeWindow1 = layero.find('iframe')[0].contentWindow;
                        iframeWindow1.layui.form.render();
                    }
                }
            });
            layui.layer.full(index);
            window.sessionStorage.setItem("index", index);
            //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
            $(window).on("resize", function () {
                layui.layer.full(window.sessionStorage.getItem("index"));
            })
        }
    }

    function editPhotographerDetail(data) {
        var title = "新增摄影师";

        if (data) {

        }
        var index = layui.layer.open({
            title: title,
            type: 2,
            content: "photographerEdit.html" + params,
            success: function (layero, index) {
                var body = layui.layer.getChildFrame('body', index);
                if (data) {
                    // store-id
                    body.find("#photographer").val(data.id);
                    body.find(".storeName").val(data.name);
                    body.find(".storeEnglishName").val(data.nameEn);
                    body.find(".storeContact").val(data.contact);
                    body.find(".contactNumber").val(data.contactNumber);
                    body.find(".storePhone").val(data.phone);

                    if (data.salesList) {
                        var saleList = data.salesList.split(',');
                        for (var index in saleList) {
                            if (saleList[index] == 1) {
                                body.find("input:checkbox[value='watch']").attr("checked", "checked");
                            } else if (saleList[index] == 2) {
                                body.find("input:checkbox[value='jewelry']").attr("checked", "checked");
                            } else if (saleList[index] == 3) {
                                body.find("input:checkbox[value='bag']").attr("checked", "checked");
                            } else if (saleList[index] == 4) {
                                body.find("input:checkbox[value='perfume']").attr("checked", "checked");
                            } else if (saleList[index] == 5) {
                                body.find("input:checkbox[value='glasses']").attr("checked", "checked");
                            }
                        }
                    }
                    body.find(".storeType").val(data.type);
                    var canAppoint = data.canAppoint ? "1" : "0";
                    body.find(".canAppoint").val(canAppoint);

                    var bannerContent = '';
                    for (var i = 1; i <= 5; i++) {
                        if (data['image' + i]) {
                            bannerContent += '<div img="' + d["image" + i] + '" class="table-cell-image" style="background-image:url(' + d["image" + i] + ')"></div>';
                        }
                    }
                    body.find("#store-banner").html(bannerContent);

                    body.find(".event-title").val(data.eventTitle);
                    body.find(".event-englishtitle").val(data.eventTitleEn);
                    body.find(".event-url").val(data.eventUrl);

                    body.find(".storeAddress").val(data.address);
                    body.find(".storeEnglishAddress").val(data.addressEn);
                    var iframeWindow = layero.find('iframe')[0].contentWindow;
                    iframeWindow.layui.form.render();
                }
            }
        });
        layui.layer.full(index);
        window.sessionStorage.setItem("index", index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize", function () {
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }

    //是否置顶
    form.on('switch(newsTop)', function (data) {
        var index = layer.msg('修改中，请稍候', {icon: 16, time: false, shade: 0.8});
        setTimeout(function () {
            layer.close(index);
            if (data.elem.checked) {
                layer.msg("置顶成功！");
            } else {
                layer.msg("取消置顶成功！");
            }
        }, 500);
    })

    //搜索【此功能需要后台配合，所以暂时没有动态效果演示】
    $(".btn_search").on("click", function () {
        photographerPage.getData(0);
    });

    //添加文章
    function addNews(edit) {
        var index = layui.layer.open({
            title: "添加文章",
            type: 2,
            content: "newsAdd.html",
            success: function (layero, index) {
                var body = layui.layer.getChildFrame('body', index);
                if (edit) {
                    body.find(".newsName").val(edit.newsName);
                    body.find(".abstract").val(edit.abstract);
                    body.find(".thumbImg").attr("src", edit.newsImg);
                    body.find("#news_content").val(edit.content);
                    body.find(".newsStatus select").val(edit.newsStatus);
                    body.find(".openness input[name='openness'][title='" + edit.newsLook + "']").prop("checked", "checked");
                    body.find(".newsTop input[name='newsTop']").prop("checked", edit.newsTop);
                    form.render();
                }
                setTimeout(function () {
                    layui.layer.tips('点击此处返回文章列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        })
        layui.layer.full(index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize", function () {
            layui.layer.full(index);
        })
    }

    $(".addNews_btn").click(function () {
        addNews();
    })

    //批量删除
    $(".delAll_btn").click(function () {
        var checkStatus = table.checkStatus('newsListTable'),
            data = checkStatus.data,
            newsId = [];
        if (data.length > 0) {
            for (var i in data) {
                newsId.push(data[i].newsId);
            }
            layer.confirm('确定删除选中的文章？', {icon: 3, title: '提示信息'}, function (index) {
                // $.get("删除文章接口",{
                //     newsId : newsId  //将需要删除的newsId作为参数传入
                // },function(data){
                tableIns.reload();
                layer.close(index);
                // })
            })
        } else {
            layer.msg("请选择需要删除的文章");
        }
    })

    //列表操作
    table.on('tool(newsList)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;

        if (layEvent === 'edit') { //编辑
            addNews(data);
        } else if (layEvent === 'del') { //删除
            layer.confirm('确定删除此文章？', {icon: 3, title: '提示信息'}, function (index) {
                // $.get("删除文章接口",{
                //     newsId : data.newsId  //将需要删除的newsId作为参数传入
                // },function(data){
                tableIns.reload();
                layer.close(index);
                // })
            });
        } else if (layEvent === 'look') { //预览
            layer.alert("此功能需要前台展示，实际开发中传入对应的必要参数进行文章内容页面访问")
        }
    });

})
