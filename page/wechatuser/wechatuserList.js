layui.config({
    base: "../../js/"
}).extend({
    "utilObj": "utilObj"
})
layui.use(['utilObj', 'form', 'laydate', 'layer', 'laypage', 'table', 'laytpl'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        utilObj = layui.utilObj,
        laypage = layui.laypage,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;
    var findUserUrl = "cms/wechatuser/findUserPage";

    var userPage = {
        getData: function (pageIndex) {
            var phone = $("#phone").val();
            var startTime = $("#startTime").val();
            var endTime = $("#endTime").val();
            var _jsonFilter = "{";
            if (phone.length > 0) {
                _jsonFilter += "search_LIKE_phone:'" + phone + "',";
            }
            if (startTime.length > 0) {
                //大于等于
                startTime += ' 00:00:00';
                _jsonFilter += "search_GTE_createTime:'" + startTime + "',";
            }
            if (endTime.length > 0) {
                //小于等于
                endTime += ' 23:59:59';
                _jsonFilter += "search_LTE_createTime:'" + endTime + "',";
            }
            if (_jsonFilter != "{") {
                _jsonFilter = _jsonFilter.substring(0, _jsonFilter.length - 1);
                _jsonFilter += "}";
            } else {
                _jsonFilter += "}";
            }
            var requestData = {
                jsonFilter: _jsonFilter,
                pageIndex: pageIndex - 1,
                pageSize: utilObj.pageSize
            };
            utilObj.showLoading();
            utilObj.ajax({
                url: findUserUrl,
                type: 'POST',
                data: requestData,
                success: function (data) {
                    //用户列表
                    if (data && data.content) {
                        userPage.tableIns = table.render({
                            elem: '#userList',
                            cellMinWidth: 95,
                            page: false,
                            height: "full-175",
                            limit: utilObj.pageSize,
                            id: "userListTable",
                            cols: [[
                                {
                                    field: 'openId',
                                    title: 'OpenId',
                                    minWidth: 100,
                                    align: "center"
                                },
                                {
                                    field: 'phone',
                                    title: '用户手机号',
                                    minWidth: 80,
                                    align: 'center'
                                },
                                {
                                    field: 'unionId',
                                    title: 'UnionId',
                                    minWidth: 100,
                                    align: 'center'
                                },
                                {
                                    field: 'nickName',
                                    title: '用户昵称',
                                    align: 'center'
                                },
                                {
                                    field: 'gender',
                                    title: '性别',
                                    align: 'center'
                                },
                                {
                                    field: 'avatar',
                                    title: '头像',
                                    minWidth: 100,
                                    minHeight: 100,
                                    align: 'center',
                                    templet: function (d) {
                                        var r = '';
                                        if (d.avatar) {
                                            // r = '<div><img src="{{d.avatar}}" style="height:48px;width:48px;line-height:48px!important;"></div>';
                                            r = '<div img="' + d.avatar + '" class="table-cell-image" style="background-image:url(' + d.avatar + ')"></div>';
                                        }
                                        return r;
                                    }
                                },
                                {
                                    field: 'createTime',
                                    title: '创建时间',
                                    align: 'center',
                                    templet: function (d) {
                                        return convertDateLongToString(d.createTime);
                                    }
                                }
                            ]],
                            data: data.content
                        });
                        laypage.render({
                            elem: 'pagination',
                            curr: pageIndex,
                            count: data.totalElements,
                            limit: utilObj.pageSize,
                            jump: function (obj, first) {
                                userPage.current = obj.curr;
                                if (!first) {
                                    userPage.getData(obj.curr);
                                }
                            }
                        });
                    }
                    utilObj.hideLoading();
                },
                error: function (err) {
                    utilObj.hideLoading();
                    alert(err.message);
                }
            });
        },
        exportData:function(data){
    		console.log(data);
    		var d={};
    		data.phone!="" && (d.phone=data.phone);
    		data.startTime!="" && (d.startTime=data.startTime);
    		data.endTime!="" && (d.endTime=data.endTime);
    		
    		utilObj.ajax({
				url: "cms/wechatuser/exportUser",
				type:'GET',
				data:d,
				success: function(data){
					if(data){
					}
					utilObj.hideLoading();
				}
			});
    	},
        init: function () {
            userPage.getData(1);
            laydate.render({
                elem: '#startTime', type: 'date'
            });
            laydate.render({
                elem: '#endTime', type: 'date'
            });
            //导出
		    form.on('submit(submit2)', function(data){
		    	if(data.field.startTime.trim()==""){
		    		layer.open({
				        type: 1
				        ,title:"提示"
				        ,offset: 'auto' 
				        ,content: '<div style="padding: 20px 20px;">如不填写开始时间，默认导出三个月内数据。</div>'
				        ,btn: '导出'
				        ,btnAlign: 'c' //按钮居中
				        ,shade: 0 //不显示遮罩
				        ,yes: function(){
				        	userPage.exportData(data.field);
				            layer.closeAll();
				        }
				    });
		    	}else{
		    		userPage.exportData(data.field);
		    	}
			    return false;
		    });
        }
    }
    userPage.init();


    /* function initStorePage() {
         var _jsonFilter = "{}";

         utilObj.ajax({
             url: "cms/store/getStorePage",
             type: 'POST',
             data: {
                 jsonFilter: _jsonFilter,
                 pageIndex: pageIndex,
                 pageSize: pageSize
             },
             success: function (data) {
                 console.log(data);
                 console.log(data.first);
             },
             error: function (err) {
                 //page/login/login.html
                 alert(err.message);
             }
         })
     }*/


    $(".search_btn").on("click", function () {
        // if ($(".searchVal").val() != '') {
        //userList  搜索如何进行查询了？
        userPage.getData(1);
        // } else {
        //     layer.msg("请输入搜索的内容");
        // }
    });
    $(".export_btn").on("click", function () {
        //导出Excel表格
    });

    function convertDateLongToString(str) {
        if (str == "" || str == null || str == undefined) {
            return "";
        } else {
            return new Date(str).format('yyyy-MM-dd hh:mm:ss');
        }
    }

    Date.prototype.format = function (format) {
        var o = {
            "M+": this.getMonth() + 1,
            "d+": this.getDate(),
            "h+": this.getHours(),
            "m+": this.getMinutes(),
            "s+": this.getSeconds(),
            "q+": Math.floor((this.getMonth() + 3) / 3),
            "S": this.getMilliseconds()
        }
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return format;
    }

});