layui.config({
    base: "../../js/"
}).extend({
    "utilObj": "utilObj"
})
layui.use(['utilObj', 'form', 'layer', 'laydate', 'table', 'laytpl', 'laypage', 'util'], function () {
    var utilObj = layui.utilObj;//自定义模块，包含ajax
    var laypage = layui.laypage;
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;

    var appointRankUrl = "/cms/report/getAppointRank";
    var appoinrRateUrl = "/cms/report/getAppointRate";
    var pagePage = {
        getData: function () {
            pagePage.getAppointRank();
            pagePage.getAppointRate();
        },
        getAppointRank: function () {
            var startTime = $("#startTime").val();
            var endTime = $("#endTime").val();
            utilObj.showLoading();
            utilObj.ajax({
                url: appointRankUrl,
                data: {
                    startDate: startTime,
                    endDate: endTime
                },
                success: function (data) {
                    console.log(data);
                    if (data) {
                        pagePage.bindAppointRank(data);
                        pagePage.renderAppointRank(data);
                    }
                    utilObj.hideLoading();
                },
                error: function (err) {
                    layer.msg(err.message);
                    utilObj.hideLoading();
                }
            });
        },
        getAppointRate: function () {
            var startTime = $("#startTime").val();
            var endTime = $("#endTime").val();
            utilObj.showLoading();
            utilObj.ajax({
                url: appoinrRateUrl,
                data: {
                    startDate: startTime,
                    endDate: endTime
                },
                success: function (data) {
                    if (data) {
                        pagePage.bindAppointRate(data);
                    }
                    utilObj.hideLoading();
                },
                error: function (err) {
                    layer.msg(err.message);
                    utilObj.hideLoading();
                }
            });
        },
        bindAppointRank: function (chartData) {
            var lineData = [];
            for (var index in chartData) {
                if (index >= 10) {
                    break;
                }
                var items = [];
                items.push(chartData[index].storeName);
                items.push(chartData[index].number);
                lineData.push(items);
            }
            Highcharts.chart('appointRank', {
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -60  // 设置轴标签旋转角度
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '到店次数'
                    }
                },
                tooltip: {
                    pointFormat: '到店次数: <b>{point.y:.0f} </b>'
                },
                series: [{
                    name: '到店次数',
                    data: lineData,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.0f}', // :.1f 为保留 1 位小数
                        y: 10
                    }
                }]
            });
        },
        bindAppointRate: function (chartData) {
            var lineData = [];
            for (var index in chartData) {
                if (chartData[index].status == 1) {
                    var items = [];
                    items.push('预约总数占比');
                    items.push(chartData[index].rate);
                    lineData.push(items);
                } else if (chartData[index].status == 2) {
                    var items = [];
                    items.push('到店总数占比');
                    items.push(chartData[index].rate);
                    lineData.push(items);
                } else if (chartData[index].status == 3) {
                    var items = [];
                    items.push('到店购买占比');
                    items.push(chartData[index].rate);
                    lineData.push(items);
                }
            }
            Highcharts.chart('appointRate', {
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                chart: {
                    type: 'funnel',
                    marginRight: 100
                },
                title: {
                    text: null,
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            crop: false,
                            overflow: 'none',
                            format: '<b>{point.name}</b> ({point.y})',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            softConnector: true
                        },
                        neckWidth: '0',
                        neckHeight: '0',
                    }
                },
                legend: {
                    enabled: false
                },
                series: [{
                    name: '百分比',
                    data: lineData
                }]
            });
        },
        renderAppointRank: function (data) {
            if (data && data.length) {
                $(".ranklist-content").show();
            } else {
                $(".ranklist-content").hide();
            }
            table.render({
                elem: '#appointRankList',
                cellMinWidth: 95,
                page: true,
                height: 315,
                limit: 7,
                id: 'appointRankListTable',
                cols: [[
                    {
                        field: 'storeName',
                        title: '店铺名称',
                        minWidth: 100,
                        align: "center"
                    },
                    {
                        field: 'number',
                        title: '预约数',
                        minWidth: 100,
                        align: "center"
                    }
                ]],
                data: data
            });
        },
        init: function () {
            Date.prototype.format = function (format) {
                var o = {
                    "M+": this.getMonth() + 1,
                    "d+": this.getDate(),
                    "h+": this.getHours(),
                    "m+": this.getMinutes(),
                    "s+": this.getSeconds(),
                    "q+": Math.floor((this.getMonth() + 3) / 3),
                    "S": this.getMilliseconds()
                }
                if (/(y+)/.test(format)) {
                    format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
                }
                for (var k in o) {
                    if (new RegExp("(" + k + ")").test(format)) {
                        format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
                    }
                }
                return format;
            }


            Highcharts.setOptions({
                colors: ['#BDA990', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
            });
            var today = new Date();
            var endDate = today;
            var startDate = new Date();
            var year = endDate.getFullYear() - 1;
            startDate.setYear(year);

            var startStr = startDate.format("yyyy-MM-dd").toString();
            var endStr = endDate.format("yyyy-MM-dd").toString();
            laydate.render({
                elem: '#startTime',
                type: 'date',
                value: startStr

            });
            laydate.render({
                elem: '#endTime',
                type: 'date',
                value: endStr
            });
            pagePage.getData();
        }
        ,
    };
    pagePage.init();


    $(".confirm_btn").on("click", function () {
        pagePage.getData(1);
    });

})