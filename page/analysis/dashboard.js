layui.config({
    base: "../../js/"
}).extend({
    "utilObj": "utilObj"
})
layui.use(['utilObj', 'form', 'layer', 'laydate', 'table', 'laytpl', 'laypage', 'util'], function () {
    var utilObj = layui.utilObj;//自定义模块，包含ajax
    var laypage = layui.laypage;
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;

    var page = {
        getData: function (pageIndex) {
            utilObj.showLoading();
            utilObj.ajax({
                url: "cms/appoint/findByConditionsPage",
                data: {
                    pageSize: utilObj.pageSize,
                    pageIndex: pageIndex
                },
                success: function (data) {
                    utilObj.hideLoading();
                },
                error: function () {
                    utilObj.hideLoading();
                }
            });
        },
        bindUserGrowth: function () {
            var chart = Highcharts.chart('userGrowth', {
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                chart: {
                    type: 'line'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                tooltip: {},
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true,
                            allowOverlap: true
                        },
                    }
                },
                series: [{
                    name: '东京',
                    data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                }]
            });
        },
        bindStoreRanking: function () {
            var chart = Highcharts.chart('storeRanking', {
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: ['非洲', '美洲', '亚洲', '欧洲', '大洋洲'],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    gridLineWidth: 0
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true,
                            allowOverlap: true // 允许数据标签重叠
                        }
                    }
                },
                series: [{
                    name: '1800 年',
                    data: [107, 31, 635, 203, 2]
                }]
            });
        },
        bindUserArea: function () {
            var map = null;
            $.getJSON('https://data.jianshukeji.com/jsonp?filename=geochina/china.json&callback=?', function (mapdata) {
                var data = [{
                    name: '北京',
                    value: 5000
                }, {
                    name: '上海',
                    value: 2000
                }, {
                    name: '广东',
                    value: 2200
                }, {
                    name: '浙江',
                    value: 1800
                }, {
                    name: '福建',
                    value: 1000
                }];
                map = new Highcharts.Map('userArea', {
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    mapNavigation: {
                        enabled: true,
                        buttonOptions: {
                            verticalAlign: 'bottom'
                        }
                    },
                    colorAxis: {
                        min: 0,
                        minColor: '#fff',
                        maxColor: '#BDA990',
                        labels: {
                            style: {
                                "color": "black", "fontWeight": "bold"
                            }
                        }
                    },
                    series: [{
                        nullColor: '#eee',
                        data: data,
                        mapData: mapdata,
                        joinBy: 'name',
                        name: '中国地图'
                    }]
                });
            });

        },
        init: function () {
            Highcharts.setOptions({
                colors: ['#BDA990', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
            });
            page.bindUserGrowth();
            page.bindStoreRanking();
            page.bindUserArea();
        },
    };
    page.init();


})