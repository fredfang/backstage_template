layui.config({
	base: "../../js/"
}).extend({
	"utilObj": "utilObj"
})

layui.use(['utilObj', 'form', 'layer', 'laydate', 'table', 'laytpl', 'laypage', 'util'], function() {
	var utilObj = layui.utilObj; //自定义模块，包含ajax
	var laypage = layui.laypage;
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery,
		laydate = layui.laydate,
		laytpl = layui.laytpl,
		table = layui.table;

	var page = {
		getData: function(pageIndex) {
			utilObj.showLoading();
			utilObj.ajax({
				url: "cms/report/getHotWordList",
				data: {},
				success: function(data) {
					utilObj.hideLoading();
					var Alyzearr = data.hotList.slice(0, 10);
					page.bindSearchanAlyze(Alyzearr);
					page.bindSearchPercentage(data);
					page.bindSearchDataTable(data);
				},
				error: function() {
					utilObj.hideLoading();
				}
			});
		},
		bindSearchanAlyze: function(Alyzedata) {
			var DataArr = [];
			Alyzedata.forEach(function(e) {
				DataArr.push([e.name, e.searchTime]);
			});
			var chart = Highcharts.chart('SearchanAlyze', {
				credits: {
					enabled: false
				},
				chart: {
					type: 'column'
				},
				title: {
					text: ''
				},
				subtitle: {
					text: ''
				},
				xAxis: {
					type: 'category',
					labels: {
						rotation: -45 // 设置轴标签旋转角度
					}
				},
				yAxis: {
					min: 0,
					title: {
						text: '次数 '
					}
				},
				legend: {
					enabled: false
				},
				tooltip: {
					pointFormat: '搜索次数: <b>{point.y:1f} 次</b>'
				},
				series: [{
					name: '总次数',
					data: DataArr,
					dataLabels: {
						enabled: true,
						rotation: -90,
						color: '#FFFFFF',
						align: 'right',
						format: '{point.y:1f}', // :.1f 为保留 1 位小数
						y: 10
					}
				}]
			});
		},
		bindSearchPercentage: function(data) {
			var arr = data.hotList.slice(0, 10);
			var PercentageArr = [];
			var Others = 1;
			for(var i = 0; i < arr.length; i++) {
				var Object = arr[i];
				var Percentage = Object.searchTime / data.wholeSearch;

				Others = Others - Percentage;

				if(i == 0)
					PercentageArr.push({
						"name": Object.name,
						"y": parseFloat(Percentage),
						"sliced": true,
						"selected": true
					});
				else
					PercentageArr.push({
						"name": Object.name,
						"y": parseFloat(Percentage)
					});
			}
			PercentageArr.push({
				"name": "其他内容",
				"y": parseFloat(Others)
			});

			Highcharts.chart('SearchPercentage', {
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false,
					type: 'pie'
				},
				title: {
					text: ''
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							format: '<b>{point.name}</b>: {point.percentage:.1f} %',
							style: {
								color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
							}
						}
					}
				},
				series: [{
					name: 'Brands',
					colorByPoint: true,
					data: PercentageArr
				}]
			});
		},
		bindSearchDataTable: function(data) {

			table.render({
				elem: '#demo',
				page: true,
				height: 315,
				limit: 7,
				cols: [
					[ //表头
						{
							field: 'name',
							title: '搜索内容',
							minWidth: 100,
							align: "center"
						}, {
							field: 'searchTime',
							title: '次数',
							minWidth: 100,
							align: "center"
						}
					]
				],
				data: data.hotList
			});
		},
		init: function() {
			Highcharts.setOptions({
				colors: ['#BDA990', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
			});
			page.getData();

		},
	};
	page.init();

})