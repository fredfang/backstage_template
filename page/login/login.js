if(localStorage.getItem("user")){
	location.href="../../index.html";
}

layui.config({
	base: "../../js/"
}).extend({
	"utilObj": "utilObj"
})
layui.use(['utilObj', 'form', 'layer', 'jquery'], function() {
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer
	$ = layui.jquery;

	//登录按钮
	form.on("submit(login)", function(data) {
		var btnlogin = $(this);
		btnlogin.text("登录中...").attr("disabled", "disabled").addClass("layui-disabled");
		var userName = data.field.username;
		var pwd = data.field.password;
		layui.utilObj.ajax({
			url: "cms/login",
			type: 'POST',
			data: {
				username: userName,
				password: pwd
			},
			success: function(data) {
				localStorage.setItem("username",userName);
				localStorage.setItem("user",JSON.stringify({
					auth:data.token,
					role:data.role,
					userId:data.userId
				}));
//				layui.sessionData('user', {
//					key: 'auth',
//					value: data.token
//				});
//				layui.sessionData('user', {
//					key: 'role',
//					value: data.role
//				});
//				layui.sessionData('user', {
//					key: 'userId',
//					value: data.userId
//				});
				btnlogin.text("登录").removeAttr("disabled").removeClass("layui-disabled");
				window.location.href = '../../index.html';
			},
			error: function(err) {
				alert(err.message);
				btnlogin.text("登录").removeAttr("disabled").removeClass("layui-disabled");
			}
		});
	})

	//表单输入效果
	$(".loginBody .input-item").click(function(e) {
		e.stopPropagation();
		$(this).addClass("layui-input-focus").find(".layui-input").focus();
	})
	$(".loginBody .layui-form-item .layui-input").focus(function() {
		$(this).parent().addClass("layui-input-focus");
	})
	$(".loginBody .layui-form-item .layui-input").blur(function() {
		$(this).parent().removeClass("layui-input-focus");
		if($(this).val() != '') {
			$(this).parent().addClass("layui-input-active");
		} else {
			$(this).parent().removeClass("layui-input-active");
		}
	})
})