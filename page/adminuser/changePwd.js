layui.config({
    base: "../../js/"
}).extend({
    "utilObj": "utilObj"
});
layui.use(['form', 'layer', 'utilObj'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        utilObj = layui.utilObj;
    var findAdminUserUrl = "cms/user/findAdminUserById";
    var updateAdminUserUrl = "cms/user/updateAdminUser";
    var currentUser;
    //添加验证规则
    form.verify({
        oldPwd: function (value, item) {
            if (value != currentUser.password) {
                return "密码错误，请重新输入！";
            }
        },
        newPwd: function (value, item) {
            if (value.length < 6) {
                return "密码长度不能小于6位";
            }
        },
        confirmPwd: function (value, item) {
            if (!new RegExp($("#oldPwd").val()).test(value)) {
                return "两次输入密码不一致，请重新输入！";
            }
        }
    });
    var user = localStorage.getItem("user");
    if (user) {
        user = JSON.parse(user);
        var requestData = {
            userId: user.userId
        }
        utilObj.showLoading();
        utilObj.ajax({
            url: findAdminUserUrl,
            type: "POST",
            data: requestData,
            success: function (data) {
                if (data) {
                    currentUser = data;
                    $('.userName').val(data.userName);
                }
                utilObj.hideLoading();
            },
            error: function (err) {
                layer.msg(err.message);
                utilObj.hideLoading();
            }
        });
    }

    $('.btn-resetinfo').click(function (data) {
        $('.oldPassword').val("");
        $('.newPassword').val("");
        $('.confirmPassword').val("");
    });
    form.on("submit(changePwd)", function (data) {
        var oldPassword = $('.oldPassword').val();
        if (oldPassword !== currentUser.password) {
            layer.msg("旧密码不正确");
            return;
        }
        var newPasswrod = $('.newPassword').val();
        var requestData = {
            userId: currentUser.id,
            userName: currentUser.userName,
            chineseName: currentUser.chineseName,
            state: currentUser.isvalid,
            password: newPasswrod,
            phone: currentUser.phone
        };
        updateUser(requestData);
    });

    function updateUser(data) {
        utilObj.showLoading();
        utilObj.ajax({
            url: updateAdminUserUrl,
            type: "POST",
            data: data,
            success: function (data) {
                utilObj.hideLoading();
                layer.msg(data);
            },
            error: function (err) {
                layer.msg(err.message);
                utilObj.hideLoading();
            }
        });
    }


});