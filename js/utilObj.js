layui.define('layer', function (exports) {
    var $ = layui.jquery;
    var utilObj = {
        pageSize: 10,
        ajaxURL: "http://127.0.0.1:8082/",
        ajax: function (params) {
            if (!params.data || (params.data && !(params.data instanceof Object))) params.data = {};
            var p = {
                url: utilObj.ajaxURL + params.url,
                data: params.data,
                async: params.async!=undefined?params.async:true,
                type: params.type ? params.type : "POST",
                dataType: "json",
                success: function (data, status, xhr) {
                    if (data.code == 1) {
                        if ($.isFunction(params.success)) params.success(data.object, status, xhr);
                    } else {
                        //						utilObj.toast(data.error);
                        if ($.isFunction(params.error)) params.error(data, status, xhr);
                    }
                },
                error: function (xhr, errorType, error) {
                    console.log(xhr);
                    if (xhr.status.toString() == "401") {
                        localStorage.removeItem("user");
                        if (self != top) {
                            top.location.href = "../../page/login/login.html";
                        }else{
                        	location.href = "./page/login/login.html";
                        }
                        return;
                    }
                    if ($.isFunction(params.error)) params.error(xhr, errorType, error);
                },
                beforeSend: function (xhr) {
//					var sessionUser = layui.sessionData('user');
//					if(sessionUser != undefined &&
//						sessionUser.auth != undefined &&
//						sessionUser.userId != undefined) {
//						var auth = sessionUser.userId + '_' + sessionUser.auth;
//						//authorization
//						xhr.setRequestHeader('authorization', auth);
//					}
                    var user = localStorage.getItem("user");
                    if (user) {
                        user = JSON.parse(user);
                        xhr.setRequestHeader("authorization", user.userId + '_' + user.auth);
                    }
                },
            };
            $.ajax(p);
        },
        gotoPage: function () {

        },
        showLoading: function () {
            window.loadIndex = layer.load(2);
        },
        hideLoading: function () {
            layer.close(window.loadIndex);
        },
        getUser:function(){
        	var u=localStorage.getItem("user");
        	if(u) u=JSON.parse(u);
        	return u;
        },
        enum: {
            appointmentsStatus: {}
        },
    };

    //输出test接口
    exports('utilObj', utilObj);
});
