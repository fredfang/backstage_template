//没有token返回登录页
// if(!localStorage.getItem("user")) location.href="./page/login/login.html";

var $,tab,dataStr,layer;
layui.config({
	base : "js/"
}).extend({
	"bodyTab" : "bodyTab",
	"utilObj":"utilObj"
})
layui.use(['bodyTab','form','element','layer','jquery','utilObj'],function(){
	var form = layui.form,
		element = layui.element;
		$ = layui.$;
    	layer = parent.layer === undefined ? layui.layer : top.layer;
		tab = layui.bodyTab({
			openTabNum : "50",  //最大可打开窗口数量
			url : "json/navs.json" //获取菜单json地址
		});

	//右上角用户名
	$(".adminName").html(localStorage.getItem("username"));


	//通过顶部菜单获取左侧二三级菜单   注：此处只做演示之用，实际开发中通过接口传参的方式获取导航数据
	function getData(json){
		$.getJSON(tab.tabConfig.url,function(data){
			if(json == "contentManagement"){
				dataStr = data.contentManagement;
				//重新渲染左侧菜单
				tab.render();
			}else if(json == "memberCenter"){
				dataStr = data.memberCenter;
				//重新渲染左侧菜单
				tab.render();
			}else if(json == "systemeSttings"){
				dataStr = data.systemeSttings;
				//重新渲染左侧菜单
				tab.render();
			}else if(json == "seraphApi"){
                dataStr = data.seraphApi;
                //重新渲染左侧菜单
                tab.render();
            }

			//根据role判断2只显示报表
//			if(localStorage.getItem("role") && localStorage.getItem("role")==2){
//				$("#navBar .layui-nav-item").eq(0).remove();
//				$("#navBar .layui-nav-item").eq(0).remove();
//			}
		})
	}
	//页面加载时判断左侧菜单是否显示
	//通过顶部菜单获取左侧菜单
	$(".topLevelMenus li,.mobileTopLevelMenus dd").click(function(){
		if($(this).parents(".mobileTopLevelMenus").length != "0"){
			$(".topLevelMenus li").eq($(this).index()).addClass("layui-this").siblings().removeClass("layui-this");
		}else{
			$(".mobileTopLevelMenus dd").eq($(this).index()).addClass("layui-this").siblings().removeClass("layui-this");
		}
		$(".layui-layout-admin").removeClass("showMenu");
		$("body").addClass("site-mobile");
		getData($(this).data("menu"));
		//渲染顶部窗口
		tab.tabMove();
	})

	//隐藏左侧导航
	$(".hideMenu").click(function(){
		if($(".topLevelMenus li.layui-this a").data("url")){
			layer.msg("此栏目状态下左侧菜单不可展开");  //主要为了避免左侧显示的内容与顶部菜单不匹配
			return false;
		}
		$(".layui-layout-admin").toggleClass("showMenu");
		//渲染顶部窗口
		tab.tabMove();
	})

	//通过顶部菜单获取左侧二三级菜单   注：此处只做演示之用，实际开发中通过接口传参的方式获取导航数据
	getData("contentManagement");

	//手机设备的简单适配
    $('.site-tree-mobile').on('click', function(){
		$('body').addClass('site-mobile');
	});
    $('.site-mobile-shade').on('click', function(){
		$('body').removeClass('site-mobile');
	});

	// 添加新窗口
	$("body").on("click",".layui-nav .layui-nav-item a:not('.mobileTopLevelMenus .layui-nav-item a')",function(){
		//如果不存在子级
		if($(this).siblings().length == 0){
			addTab($(this));
			$('body').removeClass('site-mobile');  //移动端点击菜单关闭菜单层
		}
		$(this).parent("li").siblings().removeClass("layui-nav-itemed");
	})

	//清除缓存
	$(".clearCache").click(function(){
		window.sessionStorage.clear();
        window.localStorage.clear();
        var index = layer.msg('清除缓存中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
            layer.close(index);
            layer.msg("缓存清除成功！");
        },1000);
    })

	//刷新后还原打开的窗口
    if(cacheStr == "true") {
        if (window.sessionStorage.getItem("menu") != null) {
            menu = JSON.parse(window.sessionStorage.getItem("menu"));
            curmenu = window.sessionStorage.getItem("curmenu");
            var openTitle = '';
            for (var i = 0; i < menu.length; i++) {
                openTitle = '';
                if (menu[i].icon) {
                    if (menu[i].icon.split("-")[0] == 'icon') {
                        openTitle += '<i class="seraph ' + menu[i].icon + '"></i>';
                    } else {
                        openTitle += '<i class="layui-icon">' + menu[i].icon + '</i>';
                    }
                }
                openTitle += '<cite>' + menu[i].title + '</cite>';
                openTitle += '<i class="layui-icon layui-unselect layui-tab-close" data-id="' + menu[i].layId + '">&#x1006;</i>';
                element.tabAdd("bodyTab", {
                    title: openTitle,
                    content: "<iframe src='" + menu[i].href + "' data-id='" + menu[i].layId + "'></frame>",
                    id: menu[i].layId
                })
                //定位到刷新前的窗口
                if (curmenu != "undefined") {
                    if (curmenu == '' || curmenu == "null") {  //定位到后台首页
                        element.tabChange("bodyTab", '');
                    } else if (JSON.parse(curmenu).title == menu[i].title) {  //定位到刷新前的页面
                        element.tabChange("bodyTab", menu[i].layId);
                    }
                } else {
                    element.tabChange("bodyTab", menu[menu.length - 1].layId);
                }
            }
            //渲染顶部窗口
            tab.tabMove();
        }
    }else{
		window.sessionStorage.removeItem("menu");
		window.sessionStorage.removeItem("curmenu");
	}

    //消息提醒
    setInterval(function(){
    	remindMsg();
    },300000);
    remindMsg();

    function remindMsg(){
		layui.utilObj.ajax({
			url: "cms/message/getAppointRemind",
			success: function(data){
				if(data){
					var str='';
					if(data.needDealCount){
						str+=`有${data.needDealCount}个到店预约等待处理`;
					}
					if(data.appointIdsInHour && data.appointIdsInHour instanceof Array && data.appointIdsInHour.length>0){
						if(!localStorage.getItem("arriveData")) localStorage.setItem("arriveData","{}");
						var arriveData = JSON.parse(localStorage.getItem("arriveData"));
						var count=0;
						for(var d of data.appointIdsInHour){
							if(!arriveData[d]){
								count++;
								arriveData[d]=true;
							}
						}
						localStorage.setItem("arriveData",JSON.stringify(arriveData));
						if(str!="" && count) str+='<br/><br/>';
						if(count) str+=`   <a class="remindSearch" href="javascript:void(0);" style="text-decoration: underline;">有${count}个预约即将到达预约时间,请通知店铺人员准备,点击查看</a>`;
					}
					if(str!=""){
						layer.open({
				    		title:false,
				    		btn:false,
				    		offset:'20px',
				    		area:'300px',
				    		shade:false,
						    content: str,
						    success:function(layero,index){
						    	layero.find(".remindSearch").on("click",function(){
						    		var o =$("iframe").first()[0].contentWindow;
						    		o.remindDate=new Date().format("yyyy-MM-dd hh:mm:ss") + " - " + new Date( (new Date().valueOf()+3600000) ).format("yyyy-MM-dd hh:mm:ss");
						    		o.document.querySelector(".btn_search").click();
						    	});
						    }
						});
					}
				}
			}
		});
	}

    //获取省市区信息
//  if(!layer.cityList){
//	    layui.utilObj.ajax({
//			url: "cms/appoint/getCityList",
//			async:false,
//			success: function(data){
//				if(data){
//					var cityList={};
//					layui.each(data,function(i,p){
//						cityList[p.id]={name:p.country};
//						layui.each(p.cityList,function(i,c){
//							cityList[p.id][c.id]={name:c.cityName};
//						});
//					});
//					layer.cityList=cityList;
//				}
//			}
//		});
//  }
    //退出登录
    $(".signOut").on("click",function(){
    	localStorage.clear();
    	top.location.href="./page/login/login.html";
    });
})

//打开新窗口
function addTab(_this){
	tab.tabAdd(_this);
}

//捐赠弹窗
function donation(){
	layer.tab({
		area : ['260px', '367px'],
		tab : [{
			title : "微信",
			content : "<div style='padding:30px;overflow:hidden;background:#d2d0d0;'><img src='images/wechat.jpg'></div>"
		},{
			title : "支付宝",
			content : "<div style='padding:30px;overflow:hidden;background:#d2d0d0;'><img src='images/alipay.jpg'></div>"
		}]
	})
}

//图片管理弹窗
function showImg(){
    $.getJSON('json/images.json', function(json){
        var res = json;
        layer.photos({
            photos: res,
            anim: 5
        });
    });
}


/**
* 时间对象的格式化
*/
Date.prototype.format = function(format)
{
	/*
	* format="yyyy-MM-dd hh:mm:ss";
	*/
	var o = {
	"M+" : this.getMonth() + 1,
	"d+" : this.getDate(),
	"h+" : this.getHours(),
	"m+" : this.getMinutes(),
	"s+" : this.getSeconds(),
	"q+" : Math.floor((this.getMonth() + 3) / 3),
	"S" : this.getMilliseconds()
	}

	if (/(y+)/.test(format))
	{
	format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4
	- RegExp.$1.length));
	}

	for (var k in o)
	{
	if (new RegExp("(" + k + ")").test(format))
	{
	format = format.replace(RegExp.$1, RegExp.$1.length == 1
	? o[k]
	: ("00" + o[k]).substr(("" + o[k]).length));
	}
	}
	return format;
}
Date.prototype.addDay=function(i){
	var d = new Date();
	d.setTime(this.valueOf()+(1000*60*60*24)*i);
	return d;
}
Date.prototype.addSecond=function(i){
	var d = new Date();
	d.setTime(this.valueOf()+i*1000);
	return d;
}
